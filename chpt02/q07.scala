import io.StdIn._

print("How many burgers? ")
val numBurgers = readInt()
val burgerValue:Double = 2.00
val burgerOrderCost = numBurgers * burgerValue

print("How many fries? ")
val numFries = readInt()
val friesValues:Double = 1.00
val friesOrderCost = friesValues * numFries

print("How many milkshakes? ")
val numShakes = readInt()
val shakesValue:Double = 3.00
val shakesOrderCost = numShakes * shakesValue

val subtotal:Double = burgerOrderCost + friesOrderCost + shakesOrderCost
val taxAmount:Double = 0.08
val tax: Double = subtotal * taxAmount
val finalTotal: Double = subtotal + tax


println("\n\nTasty Burgers\nReceipt\n")
println(s"Burgers: $numBurgers @ $$$burgerValue")
println(s"Fries: $numFries @ $$$friesValues")
println(s"Shakes: $numShakes @ $$$shakesValue")
println()
println(s"Subtotal: $$$subtotal")
println(s"Tax ($taxAmount%): $$$tax")
println()
println(s"Total: $$$finalTotal")

