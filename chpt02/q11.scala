import io.StdIn._
import scala.math._

print("What is the angle in degrees?\r\nAngle = ")
val launchAngleDeg = readDouble()
val launchAngleRad = launchAngleDeg.toRadians

print("What is the launch speed in m/s?\r\nLaunch speed = ")
val launchSpeed = readDouble()

val rise:Double = sin(launchAngleRad) *  launchSpeed
val run:Double = cos(launchAngleRad) * launchSpeed

val gravity = 9.8 // m/s^2
val timeToApex:Double = rise / 9.8 // result units = s
val timeToGround = timeToApex * 2
val distanceTravelled = timeToGround * run

println(s"The projectile launched at $launchAngleDeg \u00b0 ($launchAngleRad rad) at $launchSpeed m/s will travel $distanceTravelled m and land $timeToGround after launching.")