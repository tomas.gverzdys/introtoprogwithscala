val ln1:Double = 50000
val ln2: Double = 0
val ln3: Double = 0
val ln4: Double = ln1 + ln2 + ln3
val maritalStatus: String = "Single"
val ln5: Double = if (maritalStatus == "Single") 10400 else 20800
val ln6: Double = if (ln5 > ln4) 0 else ln4 - ln5
val ln7: Double = 0
val ln8a: Double = 0
val ln8b: Double = 0
val ln9: Double = ln7 + ln8a
val ln10: Double = 10000 //taken from tax table (this num is made up right now)
val ln11: Double = 0
val ln12: Double = ln10 + ln11
val ln13a: Double = if (ln9 > ln12) ln9 - ln12 else 0
val ln14: Double = if (ln12 > ln9) ln12 - ln9 else 0
val summary: String = if (ln13a > ln14) s"You receive a refund of $$$ln13a" else s"You own $$$ln14"
println(summary)
