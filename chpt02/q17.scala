import io.StdIn._

println("What is the first ingredient?")
val ing1:String = readLine().toString

println("How many/much do you need?")
val amt1:Double = readDouble()

println("What is the unit price?")
val cost1:Double = readDouble()

// and then repeat the above twice more so that you can complete the question

println(s"The cost of $amt1 of $ing1 is $$${amt1*cost1}")

