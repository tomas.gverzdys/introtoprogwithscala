val area: Double = 1500 // 1500 sq ft
val desiredSoilDepth:Double = 0.5 // 6" depth
val seedDensity:Double = 5.0/1000 //5 lb per 1000 ft

val topSoilToBuy = area * desiredSoilDepth
val seedToBuy = area * seedDensity

println(s"For a yard of $area sq ft, you will need $topSoilToBuy sq ft (@ 6 in depth) and $seedDensity lb(s) (@ 5 lb /1000 ft)")