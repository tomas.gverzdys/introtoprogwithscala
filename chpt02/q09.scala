val monthlySaved:Double = 700
val annualReturn:Double = 0.07
val annualSaved:Double = monthlySaved * 12

def growInvestment(yearlyContribution: Double, annualReturn: Double, currValue:Double, yearsToLiquid:Int): Double = {
  yearsToLiquid match {
    case x if x > 0 => growInvestment(yearlyContribution, annualReturn, (currValue * (1 + annualReturn)) + yearlyContribution, yearsToLiquid - 1)
    case x if x == 0 => currValue
    case _ => throw new IllegalArgumentException("The years to cash out didn't seem to make sense.")
  }
}

growInvestment(annualSaved, annualReturn, 0,  0)