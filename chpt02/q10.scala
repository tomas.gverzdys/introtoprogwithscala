import io.StdIn._
print("What is the weekly usage? ")
val weeklyWater = readLine().toDouble

val towerCap:Double = 20000
println(s"The town has enough water for ${towerCap/weeklyWater} weeks.")
