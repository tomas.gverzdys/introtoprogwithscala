import io.StdIn.readDouble
import scala.math.{sqrt, pow}

type point = (Double, Double)

def getPoint: point = {
  print("What is the x coordinate? ")
  val x = readDouble()
  print("What is the y coordinate? ")
  val y = readDouble()
  (x,y)
}
def distanceToOrigin(p:point): Double = {
  val (x, y) = (p._1, p._2)
  sqrt(pow(x,2) + pow(y,2))
}
def checkPointInOriginCircle(circleRadius: Double, p: point): Boolean = circleRadius >= distanceToOrigin(p)


val unitCircleRad: Double = distanceToOrigin((1,0))
val myPoint = getPoint
val inCircle:Boolean = checkPointInOriginCircle(unitCircleRad, myPoint)

print(s"Your point (${myPoint._1}, ${myPoint._2}) is ")
if (inCircle) println("in the circle") else println("outside the circle.")