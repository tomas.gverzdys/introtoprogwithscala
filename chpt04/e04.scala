import io.StdIn.readInt

def argbToInt(a:Int, r:Int, g:Int, b:Int): Int = {
  a << 24 | r << 16 | g << 8 | b
}

def conformNum(n: Int):Int = {
  n match {
    case n if n < 0 => 0
    case n if n > 255 => 255
    case _ => n
  }
}

def getValue(colour:String): Int = {
  print(s"Please enter a value for $colour: ")
  conformNum(readInt())
}

val argbInt = argbToInt(getValue("alpha"),getValue("red"),getValue("green"),getValue("blue"))
println(s"Your ARGB value is $argbInt or Ox${argbInt.toHexString} or ${argbInt.toBinaryString}")