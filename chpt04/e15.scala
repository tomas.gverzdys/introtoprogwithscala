def componentSum(n:Int):Int = {
  //max int = 2147483647 = length is 10
  val ns:String = n.toString
  val ns0: String = "0"*(10-ns.length)+ns
  ns0.substring(0,1).toInt + ns0.substring(1,2).toInt + ns0.substring(2,3).toInt + ns0.substring(3,4).toInt +
    ns0.substring(4,5).toInt + ns0.substring(5,6).toInt + ns0.substring(6,7).toInt + ns0.substring(7,8).toInt +
    ns0.substring(8,9).toInt + ns0.substring(9,10).toInt
}