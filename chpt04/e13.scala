def minOfQuad(a:Int, b:Int, c:Int, d:Int): Int = a min b min c min d

def minByIf(a:Int, b:Int, c:Int, d:Int): Int = {
  val e = if (a < b) a else b
  val f = if (c < d) c else d
  if (e < f) e else f
}
