import io.StdIn.readInt
type ARGB = (Int, Int, Int, Int)
def intToARGB(n: Int):ARGB = {
  val a: Int = (n >>> 24) & 0xff
  val r: Int = (n >>> 16) & 0xff
  val g: Int = (n >>> 8) & 0xff
  val b: Int = n & 0xff
  (a,r,g,b)
}

def getARGBInt: Int ={
  print("Please enter an ARGB int: ")
  readInt()
}

val (a, r, g, b):ARGB = intToARGB(getARGBInt)
println(s"Component values are, a: $a, r: $r, g: $g, and b: $b")