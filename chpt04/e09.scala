def toCelcius(f:Double): Double = {
  (f-32) * 5 / 9
}