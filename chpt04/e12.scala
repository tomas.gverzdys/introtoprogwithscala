type km = Double
type mi = Double
type AU = Double


def kmToMi (d:km):mi = d / 1.6
def AUtoKm (d:AU):km = {val AUconst:km = 149597870700D; d/AUconst}
def AUtoMi (d:AU):mi = kmToMi(AUtoKm(d))

