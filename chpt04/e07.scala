import io.StdIn.readInt

type ARGB255 = (Int, Int, Int, Int)
type ARGB1 = (Double, Double, Double, Double)

def intToARGB255(n: Int):ARGB255 = {
  val a: Int = (n >>> 24) & 0xff
  val r: Int = (n >>> 16) & 0xff
  val g: Int = (n >>> 8) & 0xff
  val b: Int = n & 0xff
  (a,r,g,b)
}

def getARGBInt: Int ={
  print("Please enter an ARGB int: ")
  readInt()
}

def convertTo1(n:Int): Double = n.toDouble/255

def convertARGB255toARGB1(a: ARGB255): ARGB1 = (convertTo1(a._1), convertTo1(a._2),convertTo1(a._3),convertTo1(a._4))

val currARGB255:ARGB255 = intToARGB255(getARGBInt)
val currARGB1:ARGB1 =  convertARGB255toARGB1(currARGB255)
println(s"Component values are, a: ${currARGB1._1}, r: ${currARGB1._2}, g: ${currARGB1._3}, and b: ${currARGB1._4}")