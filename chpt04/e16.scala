def validateTriangle (a: Double, b: Double, c: Double): Boolean = ((a + b + c) == 180) && ((a min b min c) > 0)

def returnKind(a: Double, b:Double, c: Double):String = {
  if (a == b && b == c) "equilateral"
  else if (a == b || b == c || c == a) "isosceles"
  else "scalene"
}

def examineTriangle(a: Double, b:Double, c: Double): String = {
  if (validateTriangle(a,b,c)) returnKind(a,b,c) else "invalid triangle"
}

val a: Double = 90
val b: Double = 60
val c: Double = 30
examineTriangle(a,b,c)
