def isLeapYear(year:Int):Boolean = {
  val div4rule = year % 4 == 0
  val centuryRule = year % 100 == 0
  val FourHundredRule = year % 400 == 0
  val pre1752 = year < 1752
  div4rule && !(centuryRule ^ FourHundredRule) && !pre1752
}

def daysInThisYear(year:Int):Int = 365 + (if (isLeapYear(year)) 1 else 0)

daysInThisYear(2016)