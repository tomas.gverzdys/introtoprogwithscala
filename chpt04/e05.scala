import io.StdIn.readDouble

def argbToInt(a:Int, r:Int, g:Int, b:Int): Int = {
  a << 24 | r << 16 | g << 8 | b
}

def conformNum0to1(n: Double):Int = {
  (n*255).round.toInt match {
    case x if x < 0 => 0
    case x if x > 255 => 255
    case x => x
  }
}

def getValue0to1(colour:String): Int = {
  print(s"Please enter a value for $colour (0.0 to 1.0): ")
  conformNum0to1(readDouble())
}

val argbInt = argbToInt(getValue0to1("alpha"),getValue0to1("red"),getValue0to1("green"),getValue0to1("blue"))
println(s"Your ARGB value is $argbInt or Ox${argbInt.toHexString} or ${argbInt.toBinaryString}")