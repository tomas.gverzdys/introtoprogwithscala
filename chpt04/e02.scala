def majority(age:Int): Boolean = {
  val ageOfMagority: Int = 19
  age>=ageOfMagority
}

def checkHeight(inches:Double): Boolean = {
  inches > 48 && inches < 74
}