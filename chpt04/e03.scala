def isPalindrome(phase: String): Boolean = {
  val modifiedPhrase = phase.toLowerCase().replace(" ","")
  modifiedPhrase == modifiedPhrase.reverse
}
