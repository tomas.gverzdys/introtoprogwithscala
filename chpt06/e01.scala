

var i = 0; val a1 = Array.fill(10){i = i + 1; i}
val a2 = Array(1,2,3,4,5,6,7,8,9,10)
val a3 = 1::2::3::4::5::6::7::8::9::10::Nil
val a4 = Array.tabulate(10)(i => i+1)
val a5 = Array.fill(10)(0); fillArray(a5,0)

def fillArray(arr:Array[Int], index:Int):Unit = {
  if (index < arr.length){
    arr(index) = index+1
    fillArray(arr,index+1)
  }
}