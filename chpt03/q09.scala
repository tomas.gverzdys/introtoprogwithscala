val argb:Int = -1

val alpha = (argb >>> 24) & 0xff
val red =  (argb >>> 16) & 0xff
val green = (argb >>> 8) & 0xff
val blue = argb & 0xff

// again another really elegant solution. you bit shift (which I predicted we would do), but what I didn't see was how to
// convert into a byte, which here, you are using bit-wise AND and passing the number 11111111 (0xff). Thus, any boolean bits
// values which are bigger than 0xff will be given a 0 since they are compared against 0 in 0xff.
