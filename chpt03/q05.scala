val year:Int = 2000

val div4rule = year % 4 == 0
val centuryRule = year % 100 == 0
val FourHundredRule = year % 400 == 0
val pre1752 = year < 1752

if ( div4rule && !(centuryRule ^ FourHundredRule) && !pre1752) "leap year" else "not a leap year"
