import io.StdIn.readInt

print("What is the x coord of your point?")
val x: Int = readInt()
print("What is the y coord of your point?")
val y: Int = readInt()

(x, y) match {
  case (0, 0) => "point is at the origin"
  case (0, _) => "point is on the x-axis"
  case (_, 0) => "point is on the y-axis"
  case (x, y) if x > 0 && y > 0 => "point is in the 1st quadrant"
  case (x, y) if x < 0 && y > 0 => "point is in the 2st quadrant"
  case (x, y) if x < 0 && y < 0 => "point is in the 3rd quadrant"
  case (x, y) if x > 0 && y < 0 => "point is in the 4th quadrant"
  case (_,_) => "Something is wrong with your numbers"
}
