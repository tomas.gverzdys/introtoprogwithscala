// a)
val age = 19
if (age >= 19) println("Age of Majority") else ("Minor")

//b)
val height = 72
if (height > 48 && height < 72) println("You may ride") else println("You may not ride.")
