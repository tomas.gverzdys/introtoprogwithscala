
import scala.math._
val centre:(Double, Double) = (0, 0) //get from user
val h: Double = centre._1
val k: Double = centre._2
val r: Double = 1                    //get from user
val A: (Double, Double) = (0, 0)     //get from user
val height: Double = 1               //get from user
val width: Double = 1                //get from user
val B: (Double, Double) = (A._1 + width, A._2)
val C: (Double, Double) = (A._1 + width, A._2 + height)
val D: (Double, Double) = (A._1, A._2 + height)

// Lets program the solution to a single line segment first, AB
val m = 0//(A._2 - B._2)/(A._1 - B._1)  // that is, slope = rise / run
val b = 0//A._2 - m*A._1                // that is, b = y - mx

// and finally, solving for the coefficients for the quadratic equation
val coef_a = 1 + pow(m,2)
val coef_b = -2*h + 2*m*(b - k)
val coef_c = pow(b-k,2)-pow(r,2)

// and stealing our code from p01, lets test to see if this functions.

val discriminant: Double = pow(coef_b, 2) - 4 * coef_a * coef_c

discriminant match {
  case dis if dis < 0 => "No roots"
  case dis if dis == 0 => {val root:Double = (-1*coef_b)/(2*coef_a); s"The root is $root"}
  case dis if dis > 0 => {val lroot: Double = ((-1*coef_b) - sqrt(dis))/(2*coef_a); val rroot: Double = ((-1*coef_b) + sqrt(dis))/(2*coef_a); s"The roots are $lroot and $rroot"}
}
