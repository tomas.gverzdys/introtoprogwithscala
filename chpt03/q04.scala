import io.StdIn.readLine
print("Enter a string to test if it is a palindrome")
val original: String = readLine()
val myString: String = original.replace(" ","").toLowerCase()
if (myString eq myString.reverse) println("This is a palindrome") else println("Not a palindrome")