val month: String = "December"
val day:Int = 31
val year: Int = 2020

val div4rule = year % 4 == 0
val centuryRule = year % 100 == 0
val FourHundredRule = year % 400 == 0
val pre1752 = year < 1752
val leapYear = div4rule && !(centuryRule ^ FourHundredRule) && !pre1752

val jan = 31
val feb = jan + 28 + (if (leapYear) 1 else 0)
val mar = feb + 31
val apr = mar + 30
val may = apr + 31
val jun = may + 30
val jul = jun + 31
val aug = jul + 31
val sep = aug + 30
val oct = sep + 31
val nov = oct + 30
val dec = nov + 31


val dayNum:Int = month match {
  case "January" => 0 + day
  case "February" => jan + day
  case "March" => feb + day
  case "April" => mar + day
  case "May" => apr + day
  case "June" => may + day
  case "July" => jun + day
  case "August" => jul + day
  case "September" => aug + day
  case "October" => sep + day
  case "November" => oct + day
  case "December" => nov + day
  case _ => 0
}

println(s"Your date is the $dayNum day of the year.")