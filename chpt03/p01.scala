// if we want, we can add teminal io here
// for the sake of ease and testing, we are just going to define vals

import scala.math._

// P01: Take the coefficients from the quadratitc equation and solve for the intercepts
// f(x) = ax^2 + bx + c
val coef_a: Double = -10
val coef_b: Double = -10
val coef_c: Double = 10

val discriminant: Double = pow(coef_b, 2) - 4 * coef_a * coef_c

discriminant match {
  case dis if dis < 0 => "No roots"
  case dis if dis == 0 => {val root:Double = (-1*coef_b)/(2*coef_a); s"The root is $root"}
  case dis if dis > 0 => {val lroot: Double = ((-1*coef_b) - sqrt(dis))/(2*coef_a); val rroot: Double = ((-1*coef_b) + sqrt(dis))/(2*coef_a); s"The roots are $lroot and $rroot"}
}