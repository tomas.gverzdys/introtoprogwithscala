/* WARNING -- INCOMPLETE ANSWER
The more I work on this problem, the more I realize that the solution I have
started will not work. Thus, I am leaving this problem for now and continuing with
the exercises in the further chapters.

WHAT _CAN_ I DO HERE?
Currently the math in this script (script is too generous a word at the moment)
is able to calculate the x-coordinates at which a line intersects a circle,
but, the line must be non-vertical.
*/


/*
3.8.5 Projects 02
Write a script that tells you whether or not a rectangle overlaps with a circle. It needs to
prompt the user for the required information for the two shapes that includes the positions and
sizes, and it prints an appropriate message based on the Boolean result.
 */
import scala.math._

// use io.StdIn.readInt to get the below values. For the sake of this
// script, I am just setting these manually.

// The the circle be defined by center (h,k) and radius (r)
val centre:(Double, Double) = (0, 0) //get from user
val h: Double = centre._1
val k: Double = centre._2
val r: Double = 1                    //get from user

// let the rectangle be defined by four vertices, A B C and D; further, let the rectangle be defined by
// the bottom left corner (A) and its height (y-axis component) and width (x-axis component)
// So, th bottom left corner will be A and, moving counter clockwise, then the bottom right B, top right, C, and top left D
// Hey user, what is the coordinate for the bottom left?
val A: (Double, Double) = (0, 0)     //get from user
val height: Double = 1               //get from user
val width: Double = 1                //get from user
val B: (Double, Double) = (A._1 + width, A._2)
val C: (Double, Double) = (A._1 + width, A._2 + height)
val D: (Double, Double) = (A._1, A._2 + height)

// So, the strategy will then be to find if any of the line segments intersect the circle. This, thankfully, turns out
// to be highschool level geometry, and there are lots of videos (of which I had to watch a few) on how to do this!
// And so, what we need to do is solve the system of equations for each of the line segments. Thus, using the equation
// for a circle (x-h)^2 + (y - k)^2 = r^2 and that of a straight line (y = mx + b), we can substitute the line equation
// into the circle equation. I have done this on paper, and the result is as follows:
// (1+m^2)x^2 + (-2h + 2m(b-k))x + (b-k)^2-r^2 = 0
// Hopefully you'll notice that this is also in the form of the quadratic equation, and we programmed a solution
// to that in question p01!

// Lets program the solution to a single line segment first, AB
val m = (A._2 - B._2)/(A._1 - B._1)  // that is, slope = rise / run
val b = A._2 - m*A._1                // that is, b = y - mx

// and finally, solving for the coefficients for the quadratic equation
val coef_a = 1 + pow(m,2)
val coef_b = -2*h + 2*m*(b - k)
val coef_c = pow(b-k,2)-pow(r,2)

// and stealing our code from p01, lets test to see if this functions.

val discriminant: Double = pow(coef_b, 2) - 4 * coef_a * coef_c

discriminant match {
  case dis if dis < 0 => "No roots"
  case dis if dis == 0 => {val root:Double = (-1*coef_b)/(2*coef_a); s"The root is $root"}
  case dis if dis > 0 => {val lroot: Double = ((-1*coef_b) - sqrt(dis))/(2*coef_a); val rroot: Double = ((-1*coef_b) + sqrt(dis))/(2*coef_a); s"The roots are $lroot and $rroot"}
}



