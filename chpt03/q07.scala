import io.StdIn.readInt

def getNumFor(colour: String): Int = {
  print(s"Please enter a value (0-255) for $colour: ")
  readInt() match {
    case x if x < 0 => 0
    case x if x > 255 => 255
    case x => x
  }
}

val alpha: Int = getNumFor("alpha")
val red: Int = getNumFor("red")
val green: Int = getNumFor("green")
val blue: Int = getNumFor("blue")

val argb: Int = (alpha << 24) | (red << 16) | (green << 8) | blue //this I stole from an answer manual for the 1st edition... its brilliant

println(s"Your number is $argb as at int, ${argb.toHexString} in hex, or ${argb.toBinaryString} in binary.")