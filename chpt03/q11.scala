import scala.math._

val x: Double = 1
val y: Double = 1

val distToOri = sqrt( pow(x,2) + pow(y,2))
println(if(distToOri <= 1) "This point is inside the unit circle" else "This is outside the unit circle")
