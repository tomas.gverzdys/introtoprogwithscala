val argb:Int = -1

val alpha:Double = ((argb >>> 24) & 0xff) / 255D
val red:Double =  ((argb >>> 16) & 0xff) / 255D
val green:Double = ((argb >>> 8) & 0xff)  / 255D
val blue:Double = (argb & 0xff) / 255D

