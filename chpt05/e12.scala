type FlipCount = (Int, Int) // (heads,tails)

def flipCoin:String = {
  util.Random.nextInt(2) match {
    case 0 => "heads"
    case 1 => "tails"
  }
}

def addCoinFlip(currScore:FlipCount, tossResult:String):FlipCount = {
  val thisScore:FlipCount = tossResult match {
    case "heads" => (1,0)
    case "tails" => (0,1)
  }
  addScores(currScore,thisScore)
}

def addScores(s1:FlipCount, s2:FlipCount):FlipCount = (s1._1 + s2._1, s1._2 + s2._2)

def tossCoins(reps:Int, currScore:FlipCount = (0,0)):FlipCount = {
  if (reps <= 0) currScore
  else tossCoins(reps-1, addCoinFlip(currScore,flipCoin))
}

def resultsText(result:FlipCount):String = s"We flipped ${result._1 + result._2} coins and found ${result._1} heads and ${result._2} tails."

resultsText(tossCoins(1000))