type Score = (Int, Int)

def scoreResult(p1:String, p2:String):Score = {
  (p1, p2) match {
    case ("paper","paper") => (0,0)
    case ("paper","scissors") => (0,1)
    case ("paper","rock") => (1,0)

    case ("scissors","paper") => (1,0)
    case ("scissors","scissors") => (0,0)
    case ("scissors","rock") => (0,1)

    case ("rock","paper") => (0,1)
    case ("rock","scissors") => (1,0)
    case ("rock","rock") => (0,0)

    case (_,_) => throw new Exception("Unexpected data was passed to 'scoreResult'")
  }
}

def checkGameOver(s:Score, maxScore:Int = 3):Boolean = if ((s._1 max s._2) >= maxScore) true else false

def getCompThrow: String = {
  val result:String = util.Random.nextInt(3) match {
    case 0 => "paper"
    case 1 => "scissors"
    case 2 => "rock"
  }
  println(s"The computer chooses $result!")
  result
}

def getUserThrow:String = {
  print("Do you choose rock, paper, or scissors: ")
  scala.io.StdIn.readLine()
}

def addScores(s1:Score, s2:Score):Score = (s1._1 + s2._1, s1._2 + s2._2)

def mainLoop: Unit = {
  def iter(currScore:Score):Score = {
    if (checkGameOver(currScore)) currScore
    else {
      val thisRound: Score = playRoundOfRPS
      println(resultMessage(thisRound))
      val newScore: Score = addScores(currScore, thisRound)
      println(currScoreMessage(newScore))
      iter(newScore)
    }
  }
  val startingScore: Score = (0,0)
  val endScore = iter(startingScore)
  println(endScoreMessage(endScore))
}

def playRoundOfRPS:Score = scoreResult(getUserThrow,getCompThrow)

def currScoreMessage(s:Score):Unit = println(s"The current score is\tYou: ${s._1}\tcomputer: ${s._2}")
def resultMessage(s:Score):String = {
  s match {
    case (1,0) => "You won that round!"
    case (0,1) => "The computer won that round."
    case (0,0) => "Tie!"
  }
}

def endScoreMessage(s:Score):String = {
  if (s._1 > s._2) "You won the game!"
  else if (s._1 < s._2) "Sorry, you lost the game."
  else "The game ended in a draw."
}

mainLoop