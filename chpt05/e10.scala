
// Note that 'primeFactors' can only handle ints greater than 1.
def primeFactors(x:Int): String = {
  def iter(n: Int, curr: Int):String = {
    if (n == curr) n.toString
    else if (n % curr == 0) curr + " x " + iter(n/curr, 2)
    else iter(n, curr + 1)
  }
  iter(x, 2)
}
