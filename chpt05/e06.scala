def printPowersOfTwos2(upTo:Int): Unit = {
  def printPowers(base: Int, exp:Int): Unit = {
    val n:Int = base * 2
    if (upTo <= n) {println(base); printPowers(n, exp - 1)}
  }
  printPowers(2,upTo)
}