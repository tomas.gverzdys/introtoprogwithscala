
def reverseString(s:String):String = {
  def iter(origString:String,newString:String = ""):String = {
    if (origString.isEmpty) newString
    else iter(origString.init, newString + origString.last)
  }
  iter(s)
}

//since this starts with a 0, Scala won't recognize it as an Int, so I've made it a String
val reverseThis:String = "07252015"
reverseString(reverseThis)

//the code for reversing an Int is the same, mind you:
def reverseInt(n:Int):Int = reverseString(n.toString).toInt