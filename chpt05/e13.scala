
def increaseAmountByRate(years:Int, amount:Double, rate:Double):Double = {
  println(s"$years\t$amount")
  if (years > 0) increaseAmountByRate(years-1, amount * (1+rate), rate)
  else amount
}

def accumulateAmount(years:Int, amount:Double, rate: Double):Double = {
  if (years > 0) amount + accumulateAmount(years - 1, increaseAmountByRate(1,amount,rate), rate)
  else 0
}

def calcFutureCosts(yearsTilStart: Int, lengthOfProgram: Int, currCost:Double, increaseRate:Double):Double = {
  val costAtStart: Double = increaseAmountByRate(yearsTilStart, currCost,increaseRate)
  accumulateAmount(lengthOfProgram,costAtStart,increaseRate)
}

calcFutureCosts(10,4,30000,0.05)