def raiseXtoY(x:Int, y:Int): Int = {
  y match {
    case z if z < 0 => throw new NumberFormatException("Unable to handle negative numbers")
    case 0 => 1
    case 1 => x
    case iter => x * raiseXtoY(x, y - 1)
  }
}

