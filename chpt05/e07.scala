def timesTable(x:Int, y:Int): Unit = {
  def iter(currX: Int, currY: Int): Unit = {
    if (currY > 0) {
      if (currX > 1) {
        print((x - currX + 1) * (y - currY + 1) + "\t"); iter(currX - 1, currY)
      }
      else {
        println((x - currX + 1) * (y - currY + 1)); iter(x, currY - 1)
      }
    }
  }
  iter(x,y)
}

def timesTableTo10s: Unit = {
  timesTable(10, 10)
}
