import io.StdIn.readLine

def inputAndCount(base:Int, func:(Int,Int)=>Int):(Int,Int) = {
  readLine() match {
    case "quit" =>
      (base, 0)
    case n =>
      val (s,c) = inputAndCount(base, func)
      (func(s, n.toInt), c+1)
    }
}

def findUserMin:Int = {
  val x:(Int, Int) = inputAndCount(Int.MaxValue, _ min _)
  x._1
}

def findUserMax:Int = {
  val x:(Int, Int) = inputAndCount(Int.MinValue, _ max _)
  x._1
}

println("Enter numbers to find the min: ")
println(s"The smallest number was ${findUserMin}")
println()
println("Enter numbers to find the max: ")
println(s"The largest number was ${findUserMax}")