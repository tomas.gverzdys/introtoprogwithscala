def isPrime(n:Int): Boolean = {
  def iter(curr: Int):Boolean = {
    if (curr == n) true
    else if (n % curr == 0) false
    else iter(curr + 1)
  }
  iter(2)
}