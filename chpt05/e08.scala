/*
The behavior of the count function, show below, can be explained by the order in which the function collapses the
nests that it created by iteratively calling itself. So, if we call 'count(4)', the first time into the loop, we
enter the if statement and then immediately call the count function again, this time as 'count(3)', this repeats until
-1 where, for the first time, we actually hit the 'println(0)' statement. This most internal function collapses,
followed by the "1" function, by the "2" and so on. Another way to look at this all is as follows:
count(count(count(count(count())))) . Thus, we need to resolve inside the brackets first, which has the value 0.
Further, I would predict that, if given a very large number, this function would spend the first half of its
existence generating nested functions, and the second half printing numbers. This would be in contrast to the
countDown function, also below, which would not only print the numbers in descending order, but also print
the numbers throughout the entire runtime of the function.
 */


def count(n:Int):Unit = {
  if (n>=0){
    count(n-1)
    println(n)
  }
}

def countDown(n:Int):Unit = {
  if (n>=0){
    println(n)
    countDown(n-1)
  }
}
