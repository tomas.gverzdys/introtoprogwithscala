def RaiseToPowerv2(x: Int, y:Int):Int = {
  y match {
    case n if n < 0 => 0
    case 0 => 1
    case 1 => x
    case n if n > 1 => if (y%2 == 0) RaiseToPowerv2(x*x, y/2) else x * RaiseToPowerv2(x, y-1)
  }
}