import io.StdIn.readLine

def findMinIter(n:Int): Int = {
  print("Enter a number or \"quit\": ")
  readLine() match {
    case "quit" => n
    case u => findMinIter(u.toInt min n)
  }
}

def findMinMain:Unit = {
  println("This script will accept ints and, once you enter 'quit', will report the smallest int entered.")
  print("Please enter your first number or \"quit\": ")
  readLine() match {
    case "quit" => println("Program terminated without numbers. No value to report.")
    case n => println(s"The smallest int entered was ${findMinIter(n.toInt)}")
  }
}

findMinMain