
def eEuclid(a:Int, b:Int):(Int, Int, Int) = {
  if (b == 0) (a, 1, 0)
  else {
    val (d, x, y) = eEuclid(b, a % b);
    (d, y, x - (a/b) * y)
  }
}

// note that to accomplish ꜖a/b˩ ("truncation of integer division for positive a and b"), I abuse scala's
// habit of dropping remainders when doing division on Ints. Thus, ꜖Int/Int˩ can be simply written as Int/Int.
// As a side note, "꜖" and "˩" are apparently called "tone letters".

def checkEuclid(a:Int, b:Int, gcd:Int, x:Int, y:Int):Boolean = {
  gcd == x*a + y*b
}

/*
val a = 99
val b = 66
val (gcd, x, y) = eEuclid(a,b)
checkEuclid(a, b, gcd, x, y)
*/

