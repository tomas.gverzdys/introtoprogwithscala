def printPowersOfTwos(upTo:Int): Unit = {
  def printPowers(base: Int, exp:Int): Unit = {
    if (exp != 0) {println(base); printPowers(base * 2, exp - 1)}
  }
  printPowers(2,upTo)
}