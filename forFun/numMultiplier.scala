/*
This program is intended to find out how many steps it takes to for any number to collapse to a single
digit (do they all collapse to one digit?) when the neighbouring pairs of numbers are multiplied by each other.

For example, 293 gives 2 * 9 = 18, 9 * 3 = 27 which yield 1827.
Then 1*8 = 8, 8*2 = 16, 2*7=14, which together gives 81614
Then we get 864
Then 3224
Then 648
Then 2432
Then 8126
Then 8212
Then 1622
Then 6124
Then 628
Then 1216
Then 226
Then 412
Then 42
Then 8 -- DONE

That was actually a pretty good number! We generated 15 numbers from our original.

Note that 0 is a valid number (even at the start of a number), so don't discard it if it occurs.
 */

type DigitList = List[Int]

def digitListToProduct(digits:DigitList):String = digits.sliding(2).map(_.product).toList.mkString

def stringToDigitList(n:String):DigitList = {
  //your number must be provided as a string
  def iter(n:String, list:DigitList):DigitList = {
    val a:Int = n.head.toInt - 48 //head gives a char, which is offset from numbers by 48
    val z:String = n.tail
    if (z.isEmpty) list :+ a
    else iter(z, list :+ a)
  }
  iter(n,Nil)
}

def collapse(n:Int):(Int, Int, Int) = {
  //takes number to act on, returns the original number, the final result, and the number of steps till collapse
  def iter(num: String, counter: Int = 0): (String, Int) = {
    val len:Int = num.length
    if (len > 1 && counter < 100 && len < 1000) iter(digitListToProduct(stringToDigitList(num)).toString, counter + 1)
    else (num, counter)
  }
  val (f,c) = iter(n.toString, 0)
  (n,f.toInt,c)
}

def printCollapse(n:Int,f:Int,c:Int):Unit = println(s"Number:\t$n\tResult:\t$f\tSteps:\t$c")

def isPrime(n:Int):Boolean = {
  def iter(counter:Int):Boolean = {
    if (counter == n) true
    else if (n % counter == 0) false
    else iter(counter + 1)
  }
  iter(2)
}

val myList = List.tabulate(100)(x => x)

val calculated = myList.map(collapse(_))
calculated.filter(_._3 > 3)
