# introToProgWithScala
## Answers for Introduction to Programming and Problem Solving Using Scala, 2nd Ed.
These are my answers to the end chapter questions found in the book
"Introduction to Programming and Problem Solving Using Scala - 2nd Ed.".

The book's homepage can be found [here](http://www.programmingusingscala.net/home/introduction-to-programming-and-problem-solving-using-scala).
This location has the videos that accompany the book but, unfortunatley, no problem answers (thus my motivation for this
repo). With that said, I have found that some solutions can be found by clicking through to the appropriate chapter from the
first edition of the book [here](http://www.programmingusingscala.net/home/chapters). Do note that there are some differences in the
chapter numbers between the first and second edition.

## Contributions from Others
I have never had a public repo for this sort of thing, so I'm a bit of a noob when it comes to this stuff.
If you feel like contributing, certainly feel free to contact me, Tomas Gverzdys, at "gverzdys@wisc.edu".

